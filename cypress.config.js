const { defineConfig } = require("cypress");

module.exports = defineConfig({
  chromeWebSecurity: false, // Esta opción habilita el modo incógnito

  // Resto de tu configuración...
  // ...

  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});