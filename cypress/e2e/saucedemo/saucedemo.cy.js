/// <reference types="cypress" />

const badName = "Carlos";
const badPassword = "CarlosElCrack";
const CorrectName = "standard_user";
const CorrectPassword = "secret_sauce";

describe('cypress - e2e - saucedemo', () => {
    beforeEach(() => {
      cy.visit('https://www.saucedemo.com/')
    });
  
    it('Should return a invalid login', () => {
      cy.get("#user-name").type(badName);
      cy.get("#password").type(badPassword);
      cy.get('#login-button').click();
      cy.get('.error-message-container.error').should('be.visible');
      cy.get('.error-message-container.error h3[data-test="error"]').should('contain.text', 'Epic sadface: Username and password do not match any user in this service');
    });

    it('Should return a valid login', () => {
      cy.get("#user-name").type(CorrectName);
      cy.get("#password").type(CorrectPassword);
      cy.get('#login-button').click();
      cy.get('#react-burger-menu-btn').should('exist');
      cy.get('#react-burger-menu-btn').click();
      cy.get('#logout_sidebar_link').click();
    });

    it('Should navigate into 3 pages', () => {
      cy.get("#user-name").type(CorrectName);
      cy.get("#password").type(CorrectPassword);
      cy.get('#login-button').click();
      cy.get('#shopping_cart_container').click();
      cy.get('#react-burger-menu-btn').click();
      cy.get('#logout_sidebar_link').click();
    });
});


